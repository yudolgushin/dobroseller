package com.dolgushin.dobroseller.webapi.responses;

public class BaseResponse<T> {
    public static final String UNEXPECTED_ERROR = "UNEXPECTED_ERROR";
    public static final String OK = "OK";
    public static final String FAILED_TO_UPLOAD_FILE = "FAILED_TO_UPLOAD_FILE";
    public String result;
    public T value;
}
