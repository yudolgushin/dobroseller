package com.dolgushin.dobroseller.webapi.responses;


import com.dolgushin.dobroseller.models.Product;

import java.util.List;

public class GetAllProductsResponse extends BaseResponse<List<Product>> {
}
