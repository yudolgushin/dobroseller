package com.dolgushin.dobroseller.webapi.retrofit;


import com.dolgushin.dobroseller.Info;
import com.dolgushin.dobroseller.webapi.responses.BaseResponse;

import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitService {
    public static RetrofitService instance;
    private final APIService api;

    public APIService getApi() {
        return api;
    }

    public RetrofitService() {
        OkHttpClient client = new OkHttpClient.Builder()
                .authenticator(
                        (route, response) -> {
                            String credential = Credentials.basic(Info.getInstance().getLogin(), Info.getInstance().getPassword());
                            return response.request().newBuilder().header("Authorization", credential).build();
                        }
                ).build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Info.getInstance().BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api = retrofit.create(APIService.class);
    }

    public static synchronized RetrofitService getInstance() {
        if (instance == null) {
            instance = new RetrofitService();
        }
        return instance;
    }

    public static <T extends BaseResponse> void request(Func1<Call<T>, APIService> callProducer, Action1<T> onSuccess, Action1<Throwable> onFail) {

        Call<T> call = callProducer.call(RetrofitService.getInstance().getApi());


        call.enqueue(new Callback<T>() {
                    @Override
                    public void onResponse(Call<T> call, Response<T> response) {
                        try {
                            T body = response.body();
                            if (BaseResponse.OK.equals(body.result)) {
                                onSuccess.call(body);
                                return;
                            }
                            onFail.call(new RuntimeException(body.result));
                        } catch (Exception e) {
                            e.printStackTrace();
                            onFail.call(e);
                        }
                    }

                    @Override
                    public void onFailure(Call<T> call, Throwable t) {
                        onFail.call(t);
                    }
                });
    }
}
