package com.dolgushin.dobroseller.webapi.requests;


import com.dolgushin.dobroseller.models.PersonInfo;

public class IncOrderItemQtyRequest {
    public String orderItemId;
    public boolean isDecrement;
    public PersonInfo clientInfo;
}
