package com.dolgushin.dobroseller.webapi.requests;


import com.dolgushin.dobroseller.models.PersonInfo;

import java.math.BigDecimal;

public class UpdatePriceRequest {
    public String orderItemId;
    public BigDecimal newPrice;
    public PersonInfo clientInfo;
}
