package com.dolgushin.dobroseller.webapi.retrofit;


import com.dolgushin.dobroseller.models.PersonInfo;
import com.dolgushin.dobroseller.webapi.requests.AddToCartRequest;
import com.dolgushin.dobroseller.webapi.requests.IncOrderItemQtyRequest;
import com.dolgushin.dobroseller.webapi.requests.UpdatePriceRequest;
import com.dolgushin.dobroseller.webapi.responses.BaseResponse;
import com.dolgushin.dobroseller.webapi.responses.GetAllProductsResponse;
import com.dolgushin.dobroseller.webapi.responses.GetCartResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface APIService {
    @GET("/seller/getAllProducts")
    Call<GetAllProductsResponse> getAllProducts();

    @GET("/seller/getShoppingCart")
    Call<GetCartResponse> getShoppingCart();

    @POST("/seller/addToCart")
    Call<BaseResponse> addToCart(@Body AddToCartRequest r);

    @POST("/seller/closeOrder")
    Call<BaseResponse> closeOrder(@Body PersonInfo r);

    @POST("/seller/clearOrder")
    Call<BaseResponse> clearOrder();

    @POST("/seller/repeatLastOrder")
    Call<BaseResponse> repeatLastOrder();

    @POST("/seller/incOrderItemQty")
    Call<BaseResponse> incOrderItemQty(@Body IncOrderItemQtyRequest r);

    @POST("/seller/updateOrderItemPrice")
    Call<BaseResponse> updateOrderItemPrice(@Body UpdatePriceRequest r);

    @POST("/seller/updateOrderItemTotalPrice")
    Call<BaseResponse> updateOrderItemTotalPrice(@Body UpdatePriceRequest r);
}
