package com.dolgushin.dobroseller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dolgushin.dobroseller.products.ProductsSellerActivity;
import com.dolgushin.dobroseller.webapi.retrofit.RetrofitService;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    EditText loginEditText;
    EditText passwordEditText;
    Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.dolgushin.dobroseller.R.layout.activity_login);

        if ((Info.getInstance().getLogin() != null) || (Info.getInstance().getPassword() != null)) {
            Intent i = new Intent(this, ProductsSellerActivity.class);
            startActivity(i);
        }

        loginEditText = findViewById(com.dolgushin.dobroseller.R.id.editTextLogin);
        loginEditText.setText("");
        passwordEditText = findViewById(com.dolgushin.dobroseller.R.id.editTextPassword);
        passwordEditText.setText("");
        loginButton = findViewById(com.dolgushin.dobroseller.R.id.buttonLogin);
        loginButton.setOnClickListener(this);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View view) {
        Info.getInstance().setLoginPass(loginEditText.getText().toString(), passwordEditText.getText().toString());

        RetrofitService.request(
                a -> a.getShoppingCart(),
                p -> {
                    Intent intent = new Intent(LoginActivity.this, ProductsSellerActivity.class);
                    Info.getInstance().setLoginPass(loginEditText.getText().toString(), passwordEditText.getText().toString());
                    startActivity(intent);
                }    ,
                t ->{
                    Toast.makeText(LoginActivity.this, "error ocurred " + t.toString(), Toast.LENGTH_LONG).show();
                }
        );

    }


}
