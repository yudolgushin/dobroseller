package com.dolgushin.dobroseller;


import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Utils {

    private static final String LOG_TAG = "Utils";

    public static final Type INTEGER_LIST_TYPE = new TypeToken<List<Integer>>() {
    }.getType();


    public static <T> T getRandom(List<T> list) {
        return list.get((int) (Math.random() * list.size()));
    }

    public static boolean isEmpty(CharSequence str) {
        if (str == null || str.length() == 0)
            return true;
        else
            return false;
    }

    public static String getRandomString(int length) {
        String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder builder = new StringBuilder();
        Random rnd = new Random();
        while (builder.length() < length) {
            int index = (int) (rnd.nextFloat() * characters.length());
            builder.append(characters.charAt(index));
        }
        return builder.toString();

    }

    public static boolean listsEquals(List list1, List list2) {
        if (list1 == null || list2 == null || list1.size() != list2.size()) {
            return false;
        }
        for (int i = 0; i < list1.size(); i++) {
            //noinspection SuspiciousMethodCalls
            if (!list2.contains(list1.get(i))) {
                return false;
            }
        }
        return true;
    }

    public static boolean isEmpty(Collection collection) {
        return collection == null || collection.isEmpty();
    }

    public static <T> void addUnique(List<T> from, List<T> to) {
        if (from == null || to == null) {
            return;
        }
        for (int i = 0; i < from.size(); i++) {
            T obj = from.get(i);
            if (!to.contains(obj)) {
                to.add(obj);
            }
        }
    }


    public static int size(Collection collection) {
        if (collection != null) {
            return collection.size();
        }
        return 0;
    }

    public static boolean isEmpty(Map map) {
        return map == null || map.isEmpty();
    }

    public static boolean equals(Object a, Object b) {
        return (a == b) || (a != null && a.equals(b));
    }

    public static void pass() {

    }

    public static <T> T ifNull(T nullable, T ifNull) {
        return nullable == null ? ifNull : nullable;
    }


    public static String sha256(String string) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");

            messageDigest.update(string.getBytes());
            return new String(messageDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "Failed";
    }
}
