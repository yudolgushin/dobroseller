package com.dolgushin.dobroseller.cart;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dolgushin.dobroseller.R;
import com.dolgushin.dobroseller.models.OrderItem;
import com.dolgushin.dobroseller.models.PersonInfo;
import com.dolgushin.dobroseller.webapi.requests.IncOrderItemQtyRequest;
import com.dolgushin.dobroseller.webapi.retrofit.APIService;
import com.dolgushin.dobroseller.webapi.retrofit.RetrofitService;
import com.dolgushin.dobroseller.webapi.requests.UpdatePriceRequest;

import java.math.BigDecimal;


public class CartActivity extends AppCompatActivity {
    private static final String CLEAR_CART_DIALOG = "Очистить корзину?";
    private static final String REPEAT_CART_DIALOG = "Повторить последний заказ?";
    TextView totalPriceCart;
    AlertDialog.Builder ab;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        this.setTitle(R.string.cart);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        totalPriceCart = findViewById(R.id.toolbarTextView);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));


        refreshOrder();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            RetrofitService.request(
                    a -> a.closeOrder(getClientInfo()),
                    p -> {
                        refreshOrder();
                        Snackbar.make(view, "Заказ закрыт", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }, t -> {
                        Toast.makeText(CartActivity.this, "error ocurred " + t.toString(), Toast.LENGTH_LONG).show();
                        ;
                    }
            );
        });


    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void refreshOrder() {


        RetrofitService.request(
                APIService::getShoppingCart,
                p -> {
                     totalPriceCart.setText(p.value.order.totalPrice.toString());
                    OrderItemsFragment oif = (OrderItemsFragment) mSectionsPagerAdapter.getItem(0);
                    oif.setOrder(p.value.order);
                    ClientInfoFragment cif = (ClientInfoFragment) mSectionsPagerAdapter.getItem(1);
                    cif.setOrder(p.value.order);
                },
                t -> {
                    Toast.makeText(CartActivity.this, "error occurred " + t.toString(), Toast.LENGTH_LONG).show();
                }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tabed, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {

            case R.id.action_celar_cart:
                ab = createDialogAlert(CLEAR_CART_DIALOG);
                ab.show();
                return true;

            case R.id.action_repeat_cart:
                ab = createDialogAlert(REPEAT_CART_DIALOG);
                ab.show();
                return true;

            case R.id.homeAsUp:
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    private AlertDialog.Builder createDialogAlert(final String param) {
        AlertDialog.Builder ab;
        ab = new AlertDialog.Builder(CartActivity.this);
        ab.setMessage(param);
        ab.setPositiveButton("Ок", (dialog, arg1) -> {

            switch (param) {
                case CLEAR_CART_DIALOG:
                    RetrofitService.request(
                            a -> a.clearOrder(),
                            p -> {
                                refreshOrder();

                            },
                            t -> {
                                Toast.makeText(CartActivity.this, "error occurred " + t.toString(), Toast.LENGTH_SHORT).show();
                            }
                    );
                    Toast.makeText(CartActivity.this, "Корзина очищена",
                            Toast.LENGTH_SHORT).show();
                    break;
                case REPEAT_CART_DIALOG:
                    RetrofitService.request(
                            a -> a.repeatLastOrder(),
                            p -> {
                                refreshOrder();
                            },
                            t -> {
                                Toast.makeText(CartActivity.this, "error ocurred " + t.toString(), Toast.LENGTH_LONG).show();
                            }
                    );
                    Toast.makeText(CartActivity.this, "Последний закрытй заказ",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        });
        ab.setNegativeButton("Отмена", (dialog, arg1) -> {
        });
        return ab;
    }


    public void onIncrementClick(final boolean isDecrement, final OrderItem orderItem) {
        IncOrderItemQtyRequest r = new IncOrderItemQtyRequest();
        r.isDecrement = isDecrement;
        r.orderItemId = orderItem.id;
        r.clientInfo = getClientInfo();

        RetrofitService.request(
                a -> a.incOrderItemQty(r),
                p -> {
                    refreshOrder();
                    Toast.makeText(CartActivity.this, "Количество изменено", Toast.LENGTH_SHORT).show();
                },
                t -> {
                    Toast.makeText(CartActivity.this, "error occurred " + t.toString(), Toast.LENGTH_SHORT).show();
                }
        );
    }

    private PersonInfo getClientInfo() {
        return mSectionsPagerAdapter.clientInfoFragment.getClientInfo();
    }

    public void onChangePriceClick(OrderItem orderItem) {
        ab = createDialogAlertWithEditText(orderItem);
        ab.show();
    }

    private AlertDialog.Builder createDialogAlertWithEditText(final OrderItem orderItem) {
        final AlertDialog.Builder ab;
        ab = new AlertDialog.Builder(CartActivity.this);
        ab.setMessage("Измените стоимость ");
        final EditText input = new EditText(CartActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setRawInputType(Configuration.KEYBOARD_12KEY);
        ab.setView(input);
        input.setText(orderItem.product.price.toString());

        ab.setPositiveButton("Ок", (dialog, arg1) -> {
            UpdatePriceRequest r = new UpdatePriceRequest();
            r.orderItemId = orderItem.id;
            r.clientInfo = getClientInfo();
            r.newPrice = new BigDecimal(input.getText().toString());
            RetrofitService.request(
                    a -> a.updateOrderItemPrice(r),
                    p -> {
                        refreshOrder();
                        Toast.makeText(CartActivity.this, "Цена изменина", Toast.LENGTH_SHORT).show();
                    },
                    t -> {
                        Toast.makeText(CartActivity.this, "Цена изменина", Toast.LENGTH_SHORT).show();
                    }
            );


        });
        ab.setNegativeButton("Отмена", (dialog, arg1) -> {
        });
        return ab;
    }

    public void onChangeTotalPriceClick(OrderItem orderItem) {
        ab = createDialogAlertWithEditTextTotaL(orderItem);
        ab.show();
    }

    private AlertDialog.Builder createDialogAlertWithEditTextTotaL(final OrderItem orderItem) {
        final AlertDialog.Builder ab;
        ab = new AlertDialog.Builder(CartActivity.this);

        ab.setMessage("Измените сумму ");
        final EditText input = new EditText(CartActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setRawInputType(Configuration.KEYBOARD_12KEY);
        ab.setView(input);
        input.setText(orderItem.totalPrice.toString());

        ab.setPositiveButton("Ок", (dialog, arg1) -> {
            UpdatePriceRequest r = new UpdatePriceRequest();
            r.orderItemId = orderItem.id;
            r.clientInfo = getClientInfo();
            r.newPrice = new BigDecimal(input.getText().toString());
          RetrofitService.request(
                  a -> a.updateOrderItemTotalPrice(r),
                  p -> {

                      refreshOrder();
                      Toast.makeText(CartActivity.this, "Cумма изменина", Toast.LENGTH_SHORT).show();},
                  t ->{Toast.makeText(CartActivity.this, "error occurred " + t.toString(), Toast.LENGTH_LONG).show();}
          );

           /* WebWorker.getInstance().updatePrice(UPDATE_TOTAL_PRICE_ORDER_ITEM, orderItem, getClientInfo(), inputString, () -> {
                refreshOrder();
                Toast.makeText(CartActivity.this, "Cумма изменина", Toast.LENGTH_SHORT).show();
            }, e -> Toast.makeText(CartActivity.this, "error occurred " + e.toString(), Toast.LENGTH_LONG).show());*/


        });
        ab.setNegativeButton("Отмена", (dialog, arg1) -> {
        });
        return ab;
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private OrderItemsFragment orderItemsFragment;
        private ClientInfoFragment clientInfoFragment;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    if (orderItemsFragment == null) {
                        orderItemsFragment = new OrderItemsFragment();
                    }
                    return orderItemsFragment;
                case 1:
                    if (clientInfoFragment == null) {
                        clientInfoFragment = new ClientInfoFragment();
                    }
                    return clientInfoFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
