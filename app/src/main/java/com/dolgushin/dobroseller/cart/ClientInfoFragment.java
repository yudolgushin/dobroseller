package com.dolgushin.dobroseller.cart;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.dolgushin.dobroseller.R;
import com.dolgushin.dobroseller.models.Order;
import com.dolgushin.dobroseller.models.PersonInfo;

public class ClientInfoFragment extends Fragment {
    private Order order;
    private EditText firstName;
    private EditText lastName;
    private EditText patronymic;
    private EditText phone;
    private EditText email;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cart_client_info, container, false);
    }

    private void refreshOrder() {
        if (order != null && firstName != null) {
            firstName.setText(order.clientInfo.firstName);
            lastName.setText(order.clientInfo.lastName);
            patronymic.setText(order.clientInfo.patronymic);
            phone.setText(order.clientInfo.phone);
            email.setText(order.clientInfo.email);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        firstName = view.findViewById(R.id.client_first_name_et);
        lastName = view.findViewById(R.id.client_last_name_et);
        patronymic = view.findViewById(R.id.client_patronymic_et);
        phone = view.findViewById(R.id.client_phone_et);
        email = view.findViewById(R.id.client_email_et);

        refreshOrder();
    }

    public void setOrder(Order order) {
        this.order = order;
        refreshOrder();
    }

    public PersonInfo getClientInfo() {
        PersonInfo returnInfo = new PersonInfo(
                firstName.getText().toString(),
                patronymic.getText().toString(),
                lastName.getText().toString(),
                email.getText().toString(),
                phone.getText().toString());
        return returnInfo;
    }
}
