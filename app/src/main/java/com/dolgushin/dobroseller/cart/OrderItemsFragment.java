package com.dolgushin.dobroseller.cart;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dolgushin.dobroseller.R;
import com.dolgushin.dobroseller.models.Order;

public class OrderItemsFragment extends Fragment {
    private Handler handler = new Handler();
    Order order;
    private CartAdapter adapter;
    CartActivity activity;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cart_order_item, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view_cart);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        activity = ((CartActivity) getActivity());
        adapter = new CartAdapter(activity);
        recyclerView.setAdapter(adapter);

        refreshAdapter();
    }

    private void refreshAdapter() {
        if (adapter != null && order != null) {
            adapter.setItems(order.items);
        }
    }

    public void setOrder(Order order) {
        this.order = order;
        refreshAdapter();
    }

    public CartAdapter getAdapter() {
        return adapter;
    }
}
