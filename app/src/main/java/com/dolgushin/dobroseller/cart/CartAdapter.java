package com.dolgushin.dobroseller.cart;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.dolgushin.dobroseller.R;
import com.dolgushin.dobroseller.models.OrderItem;

import java.util.ArrayList;
import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {
    List<OrderItem> list = new ArrayList<>();
    CartActivity activity;

    public CartAdapter(CartActivity activity) {
        this.activity = activity;
    }

    @Override
    public CartAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_order_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CartAdapter.ViewHolder holder, int position) {

        holder.bind(list.get(position));
    }

    public void setItems(List<OrderItem> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView nameProduct;
        TextView codeProduct;
        TextView qtyProduct;
        TextView priceProduct;
        TextView totalPriceProduct;

        Button incButton;
        Button decButton;
        Button changePrice;
        Button changeTotalPrice;
        private OrderItem orderItem;

        public ViewHolder(View itemView) {
            super(itemView);
            nameProduct = itemView.findViewById(R.id.product_in_cart_name);
            codeProduct = itemView.findViewById(R.id.product_in_cart_code);
            qtyProduct = itemView.findViewById(R.id.product_in_cart_qty);
            priceProduct = itemView.findViewById(R.id.product_in_cart_price);
            totalPriceProduct = itemView.findViewById(R.id.product_in_cart_total_price);

            incButton = itemView.findViewById(R.id.button_increment_qty);
            decButton = itemView.findViewById(R.id.button_decrement_qty);

            changePrice = itemView.findViewById(R.id.button_change_price);
            changeTotalPrice = itemView.findViewById(R.id.button_change_total_price);

            changePrice.setOnClickListener(v -> activity.onChangePriceClick(orderItem));
            changeTotalPrice.setOnClickListener(v -> activity.onChangeTotalPriceClick(orderItem));
            decButton.setOnClickListener(v -> activity.onIncrementClick(true, orderItem));
            incButton.setOnClickListener(v -> activity.onIncrementClick(false, orderItem));

        }

        public void bind(OrderItem orderItem) {
            this.orderItem = orderItem;


            nameProduct.setText(orderItem.product.name);
            codeProduct.setText(orderItem.product.code);
            qtyProduct.setText(orderItem.qty + "");
            priceProduct.setText(orderItem.product.price.toString());
            totalPriceProduct.setText(orderItem.totalPrice.toString());
        }
    }
}
