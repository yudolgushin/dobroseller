package com.dolgushin.dobroseller.models;

import com.dolgushin.dobroseller.Utils;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Order {
    public static final Type LIST_TYPE = new TypeToken<List<Order>>() {
    }.getType();
    /**
     * Used as id/primary key
     */
    public long orderNumber;
    public long closeDateTs;
    public Seller seller;
    public BigDecimal totalPrice = new BigDecimal(0);
    public PersonInfo clientInfo;

    public List<OrderItem> items = new ArrayList<>();

    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");


    public Order(long orderNumber, Date closeDateTs, PersonInfo clientInfo) {
        this.orderNumber = orderNumber;
        this.closeDateTs = closeDateTs.getTime();


        this.clientInfo = Utils.ifNull(clientInfo, new PersonInfo());
    }

    public Order(Seller seller) {
        setSeller(seller);

        this.clientInfo = new PersonInfo();
    }

    public void addProduct(Product product) {

        items.add(new OrderItem(product, 1));
        recalcTotal();

    }

    private void recalcTotal() {
        totalPrice = BigDecimal.ZERO;
        for (OrderItem item : items) {
            totalPrice = totalPrice.add(item.totalPrice);
        }
    }

    public void setSeller(Seller seller) {
        this.seller = seller.safeCopy();
    }

    public String getMessageForTl() {
        String msg = "Закрыт заказ №" + orderNumber + "\n" +
                "Дата закрытия " + simpleDateFormat.format(new Date(closeDateTs)) + "\n" +
                "Итоговая сумма " + totalPrice.intValue() + " \u20BD \n" +
                "Продавец " + (seller != null ? seller.getInfoForTl() : "");

        return msg;
    }
}
