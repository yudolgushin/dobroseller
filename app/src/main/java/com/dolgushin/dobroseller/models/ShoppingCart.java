package com.dolgushin.dobroseller.models;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.List;

public class ShoppingCart {

    public static final Type LIST_TYPE = new TypeToken<List<ShoppingCart>>() {
    }.getType();

    public Order order;

    public String sellerId;


    public ShoppingCart(Seller seller) {
        this.order = new Order(seller);
        this.sellerId = seller.id;
    }

    public void addOrderItem(Product product) {
        order.items.add(new OrderItem(product, 1));
        recalcTotalPrice();
    }

    public void recalcTotalPrice() {
        order.totalPrice = BigDecimal.ZERO;
        for (OrderItem item : order.items) {
            order.totalPrice = order.totalPrice.add(item.totalPrice);
        }
    }

    public void clear() {
        this.order = new Order(order.seller);
    }

    public void setClientInfo(PersonInfo clientInfo) {
        order.clientInfo = clientInfo;
    }

    public void updateOrder(Order order) {
        this.order = order;
    }
}
