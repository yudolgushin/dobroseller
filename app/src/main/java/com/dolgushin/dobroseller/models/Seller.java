package com.dolgushin.dobroseller.models;

import com.dolgushin.dobroseller.Utils;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;
import java.util.UUID;

public class Seller {

    public static final Type LIST_TYPE = new TypeToken<List<Seller>>() {
    }.getType();

    public String id;
    public String login;
    public String passwordHash;
    public boolean isAdmin;

    public PersonInfo info;
    public String newPassword;


    public Seller(String login, String password, boolean isAdmin, PersonInfo infop) {
        this.login = login;
        this.passwordHash = Utils.sha256(password);
        this.isAdmin = isAdmin;
        this.id = UUID.randomUUID().toString();
        this.info = Utils.ifNull(infop, new PersonInfo());
    }

    public Seller() {
    }

    public Seller(String login, String password) {
        this(login, password, false, null);
    }

    public Seller safeCopy() {
        Seller seller = new Seller();
        seller.id = this.id;
        seller.login = this.login;
        seller.info = this.info;
        return seller;
    }

    public void handleNewPassword() {
        passwordHash = null;
        if (newPassword != null) {
            if (!newPassword.isEmpty()) {
                passwordHash = Utils.sha256(newPassword);
            }
            newPassword = null;
        }
    }

    public Seller clean() {
        isAdmin = false;
        passwordHash = null;
        return this;
    }

    public String getInfoForTl() {
        String msg = login;
        if (info != null) {
            if (!Utils.isEmpty(info.lastName)) {
                msg += " " + info.lastName;
            }

            if (!Utils.isEmpty(info.firstName)) {
                msg += " " + info.firstName;
            }

            if (!Utils.isEmpty(info.patronymic)) {
                msg += " " + info.patronymic;
            }
        }
        return msg;
    }
}
