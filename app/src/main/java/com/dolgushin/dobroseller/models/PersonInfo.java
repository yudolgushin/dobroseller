package com.dolgushin.dobroseller.models;

public class PersonInfo {
    public String firstName;
    public String patronymic;  // отчество
    public String lastName; // фамилия
    public String email;
    public String phone;

    public PersonInfo(String firstName, String patronymic, String lastName, String email, String phone) {

        this.firstName = firstName;
        this.patronymic = patronymic;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
    }

    public PersonInfo() {

    }
}
