package com.dolgushin.dobroseller.models;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.UUID;

public class OrderItem {
    public String id;
    public Product product;
    public int qty;
    public BigDecimal totalPrice;

    public OrderItem(Product product, int qty) {
        id = UUID.randomUUID().toString();
        this.product = product;
        this.qty = qty;
         calcTotalPrice();
    }

    public void calcTotalPrice() {
        totalPrice = product.price.multiply(new BigDecimal(qty)).setScale(0, RoundingMode.HALF_UP);
    }
}
