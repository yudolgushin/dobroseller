package com.dolgushin.dobroseller.models;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public class Product {

    public static final Type LIST_TYPE = new TypeToken<List<Product>>() {
    }.getType();

    public String id;
    public String code;
    public String name;
    public String imgId;
    public BigDecimal price;
    public int qty;
    public int sortNumber;


    public void setSortNumber(int sortNumber) {
        this.sortNumber = sortNumber;
    }

    public Product(String code, String name, BigDecimal price, int qty, String imgId) {
        this.code = code;
        this.name = name;
        this.price = price.setScale(2, BigDecimal.ROUND_HALF_DOWN);
        this.qty = qty;
        this.imgId = imgId;
        this.id = UUID.randomUUID().toString();
    }

    public Product() {
        this("", "", new BigDecimal(1000), 10, null);
    }
}
