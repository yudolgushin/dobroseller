package com.dolgushin.dobroseller;


import android.content.Context;
import android.content.SharedPreferences;

public class Info {

    public static final String BASE_URL = "http://192.168.100.8:8080";
    private static Info instance;

    public String getLogin() {
        return getPrefs().getString("login", null);
    }

    public void setLoginPass(String login, String password) {
        getPrefs().edit().putString("login", login).putString("password", password).apply();
    }

    private SharedPreferences getPrefs() {
        return MyApp.getInstance().getSharedPreferences("default", Context.MODE_PRIVATE);
    }

    public String getPassword() {
        return getPrefs().getString("password", null);
    }

    public static synchronized Info getInstance() {
        if (instance == null) {
            instance = new Info();
        }
        return instance;
    }


}