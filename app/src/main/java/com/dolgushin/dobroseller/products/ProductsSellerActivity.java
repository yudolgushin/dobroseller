package com.dolgushin.dobroseller.products;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.dolgushin.dobroseller.Info;
import com.dolgushin.dobroseller.LoginActivity;
import com.dolgushin.dobroseller.R;
import com.dolgushin.dobroseller.cart.CartActivity;
import com.dolgushin.dobroseller.models.Product;
import com.dolgushin.dobroseller.webapi.requests.AddToCartRequest;
import com.dolgushin.dobroseller.webapi.retrofit.RetrofitService;

public class ProductsSellerActivity extends AppCompatActivity {
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_seller);
        setTitle(R.string.products);


        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        final ProductsAdapter adapter = new ProductsAdapter(this);
        recyclerView.setAdapter(adapter);
        RetrofitService.request(
                a -> a.getAllProducts(),
                p -> {
                    adapter.setItems(p.value);
                },
                t -> {
                    Toast.makeText(ProductsSellerActivity.this, "error occurred" + t.toString(), Toast.LENGTH_SHORT).show();
                }
        );

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_products, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;
        switch (id) {
            case R.id.action_to_cart_icon:
                intent = new Intent(this, CartActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_settings:
                Info.getInstance().setLoginPass(null, null);
                intent = new Intent(this, LoginActivity.class);
                this.finish();
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }

    public void onProductClick(final Product product) {
        AddToCartRequest r = new AddToCartRequest();
        r.productId = product.id;

        RetrofitService.request(
                a -> a.addToCart(r),
                p -> {
                    Toast.makeText(context, "Товар добавлен в корзину", Toast.LENGTH_SHORT).show();
                },
                t -> {
                    Toast.makeText(ProductsSellerActivity.this, "error occurred " + t.toString(), Toast.LENGTH_LONG).show();
                }
        );
    }
}
