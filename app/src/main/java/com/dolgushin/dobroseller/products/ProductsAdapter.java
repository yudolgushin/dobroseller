package com.dolgushin.dobroseller.products;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dolgushin.dobroseller.Info;
import com.dolgushin.dobroseller.R;
import com.dolgushin.dobroseller.models.Product;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {
    private List<Product> list = new ArrayList<>();
    private ProductsSellerActivity activity;

    public ProductsAdapter(ProductsSellerActivity activity) {
        this.activity = activity;
    }

    @Override
    public ProductsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.recycler_product_item, parent, false);
        return new ViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(ProductsAdapter.ViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setItems(List<Product> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgProductImageView;
        TextView nameProductTextView;
        TextView priceProductTextView;
        TextView qtyProductTextView;
        ProductsAdapter adapter;
        Product product;

        public ViewHolder(final View itemView, ProductsAdapter adapter) {
            super(itemView);
            imgProductImageView = itemView.findViewById(R.id.img_product);
            nameProductTextView = itemView.findViewById(R.id.name_product);
            priceProductTextView = itemView.findViewById(R.id.price_product);
            qtyProductTextView = itemView.findViewById(R.id.qty_product);
            this.adapter = adapter;

            itemView.setOnClickListener(v -> activity.onProductClick(product));
        }

        public void bind(Product product) {
            this.product = product;
            Picasso p = Picasso.with(activity);
            p.setLoggingEnabled(true);
            p.load(Info.getInstance().BASE_URL + "/img/" + product.imgId).into(imgProductImageView);
            nameProductTextView.setText(product.name);
            priceProductTextView.setText(product.price.toString() + " ₽");
            qtyProductTextView.setText(product.qty + " pc");
        }
    }
}
